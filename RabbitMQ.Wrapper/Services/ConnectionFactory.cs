﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Services
{
    public class ConnectionFactory
    {
        public IConnection CreateConnection(string hostName)
        {
            return new Client.ConnectionFactory
            {
                HostName = hostName
            }.CreateConnection();
        }
    }
}

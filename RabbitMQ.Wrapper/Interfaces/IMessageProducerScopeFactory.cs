﻿using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper.Interfaces
{
    interface IMessageProducerScopeFactory
    {
        IMessageProducerScope Open(MessageScopeSettings messageScopeSettings);
    }
}

﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Services;
using System;
using System.Text;
using System.Threading;

namespace Pinger
{
    class Program
    {
        private static IMessageProducerScope _messageProducerScope;
        private static IMessageConsumerScope _messageConsumerScope;
        static void Main(string[] args)
        {
            Console.WriteLine("Pinger is running now");

            IConnectionFactory connectionFactory = new RabbitMQ.Client.ConnectionFactory();
            connectionFactory.CreateConnection("localhost");

            var messageProducerScopeFactory = new MessageProducerScopeFactory(connectionFactory);
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ping_pong",
                ExchangeType = ExchangeType.Direct,
                RoutingKey = "ping"
            });

            var messageConsumerScopeFactory = new MessageConsumerScopeFactory(connectionFactory);
            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "ping_pong",
                ExchangeType = ExchangeType.Direct,
                QueueName = "ponq_queue",
                RoutingKey = "pong"
            });

            _messageConsumerScope.MessageConsumer.Received += GetMessage;

            _messageProducerScope.MessageProducer.Send("Ping");

            Console.ReadKey();
        }

        private static void GetMessage(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine(message + " received at " + DateTime.Now);

            Thread.Sleep(2500);
            _messageConsumerScope.MessageConsumer.Connect();

            _messageProducerScope.MessageProducer.Send("Ping");
        }
    }
}

﻿using System;
using System.Text;
using System.Threading;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Services;

namespace Ponger
{
    class Program
    {
        private static IMessageProducerScope _messageProducerScope;
        private static IMessageConsumerScope _messageConsumerScope;
        static void Main(string[] args)
        {

            Console.WriteLine("Ponger is running");

            IConnectionFactory connectionFactory = new RabbitMQ.Client.ConnectionFactory();
            connectionFactory.CreateConnection("localhost");

            var messageProducerScopeFactory = new MessageProducerScopeFactory(connectionFactory);
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ping_pong",
                ExchangeType = ExchangeType.Direct,
                RoutingKey = "pong"
            });

            var messageConsumerScopeFactory = new MessageConsumerScopeFactory(connectionFactory);
            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSettings
            {
                ExchangeName = "ping_pong",
                ExchangeType = ExchangeType.Direct,
                QueueName = "pinq_queue",
                RoutingKey = "ping"
            });

            _messageConsumerScope.MessageConsumer.Received += GetMessage;

            Console.ReadKey();
        }

        private static void GetMessage(object sender, BasicDeliverEventArgs args)
        {
            var body = args.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine(message + "received  at " + DateTime.Now);

            Thread.Sleep(2500);
            _messageConsumerScope.MessageConsumer.Connect();


            _messageProducerScope.MessageProducer.Send("Pong");
        }
    }
}
